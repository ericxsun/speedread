start = NORMAL;

define {
  ASCII             [\U000000-\U0000FF]
  SUPPORTED         [\U000000-\U00FFFF]
  RANGE             {SUPPORTED}
  ANYTHING          [: intersection([\U000000-\UFFFFFF], {RANGE}) :]
  SPACE             [: intersection(\P{WSpace}, {RANGE}) :]
  Letters           [: intersection(\P{Alpha}, {RANGE}) :]
  PUNCT             [: intersection(\G{P}, {RANGE}) :]
  NUMBERS           [: intersection(\P{Numeric_Type=Decimal}, {RANGE}) :]
  CUR               [: intersection(\G{Sc}, {RANGE}) :]
  SYM               [: intersection(\G{S}, {RANGE}) :]
  PUNCT_SYM         [: union({PUNCT}, {SYM}) :]
  ALNUM             [: union({Letters}, {NUMBERS}):]
  CAP               [: intersection({Letters}, \P{Upper}) :]

  PERIOD            [\.]
  CONTRACTION       \C{('(ll|re|ve|s|m|d)|n't)}
  Line_Space        [\r\n\v]
  Sep_Space         [ \t\b\a]
  WORD_PUNCT        [\.&\-,'/]
  MOST_PUNCT        [: difference({PUNCT_SYM}, {WORD_PUNCT}):]
  CHARS             [:  union({ALNUM}, {WORD_PUNCT}) :]
  CLOSE             [,\.)"!?]
  EXT_SENT_END      [?!]
  SENT_END          [?\.!]
  CLAUSE_END        [',]
  FOLLOWERS         [: union([")'}], {Sep_Space}) :]
  END               ({SENT_END}|{CLAUSE_END})*{FOLLOWERS}*{SPACE}
  
  ABMONTH           Jan|Feb|Mar|Apr|Jun|Jul|Aug|Sep|Sept|Oct|Nov|Dec
  ABDAYS            Mon|Tue|Tues|Wed|Thu|Thurs|Fri
  ABSTATE           Ala|Ariz|[A]rk|Calif|Colo|Conn|Dak|Del|Fla|Ga|[I]ll|Ind|Kans?|Ky|La|[M]ass|Md|Mich|Minn|[M]iss|Mo|Mont|Neb|Nev|Okla|[O]re|Pa|Penn|Tenn|Tex|Va|Vt|[W]ash|Wisc?|Wyo
  ACRO              [A-Za-z](\.[A-Za-z])+|(Canada|Sino|Korean|EU|Japan|non)-U\.S|U\.S\.-(U\.K|U\.S\.S\.R)
  ABTITLE           Mr|Mrs|Ms|[M]iss|Drs?|Profs?|Sens?|Reps?|Attys?|Lt|Col|Gen|Messrs|Govs?|Adm|Rev|Maj|Sgt|Cpl|Pvt|Mt|Capt|Ste?|Ave|Pres|Lieut|Hon|Brig|Co?mdr|Pfc|Spc|Supts?|Det
  ABPTIT            Jr|Sr|Bros|(Ed|Ph)\.D|Blvd|Rd|Esq
  ABCOMP            Inc|Cos?|Corp|Pp?tys?|Ltd|Plc|Bancorp|Dept|Bhd|Assn|Univ|Intl|Sys
  ABCOMP2           Invt|Elec|Natl|M[ft]g
  ABNUM             Nos?|Prop|Ph|tel|est|ext|sq|ft

  ABBREV1           ({ABMONTH}|{ABDAYS}|{ABSTATE}|{ABCOMP}|{ABNUM}|{ABPTIT}|etc|al|seq)\.
  ABBREV4           [A-Za-z]|{ABTITLE}|vs|Alex|Wm|Jos|Cie|a\.k\.a|cf|TREAS|{ACRO}|{ABCOMP2}
  ABBREV2           {ABBREV4}\.
  ACRONYM           ({ACRO})\.
  ABBREV            ({ABBREV1}|{ABBREV2}|{ACRONYM})
}

token {
  UNKNOWN;
  WORD;
  NUM;
  END_WORD;
  CLAUSE_END;
  SENTENCE_END;
  PUNCT;
  ABBREV;
  CONTRACTION;
  MIXED;
  CURRENCY;
  DOTS;
  NEW_LINE;
  FOLLOWER;
}

mode NORMAL:
<skip: [ \t\r\n]>
{
  {ALNUM}+/{SPACE}     =>  QUEX_TKN_WORD(Lexeme);
  {MOST_PUNCT}         =>  QUEX_TKN_PUNCT(Lexeme);
  {CAP}+{CUR}          =>  QUEX_TKN_CURRENCY(Lexeme);
  {PERIOD}+/{SPACE}+   =>  QUEX_TKN_DOTS(Lexeme);

  {CHARS}+/{CONTRACTION}{END}       =>  QUEX_TKN_CONTRACTION(Lexeme);
  {CHARS}+/{EXT_SENT_END}({FOLLOWERS}*{SPACE}|{SPACE}{PERIOD}+)    {
         self_send1(QUEX_TKN_SENTENCE_END, Lexeme);
         self_enter_mode(&NEW_SENTENCE);
         RETURN;
                                                                    }
  {CHARS}+/{PERIOD}({FOLLOWERS}*{Line_Space}|{SPACE}{PERIOD}+)     {
         self_send1(QUEX_TKN_SENTENCE_END, Lexeme);
         self_enter_mode(&NEW_SENTENCE);
          RETURN;
                                                                    }

  {CHARS}+/{CLAUSE_END}{FOLLOWERS}*{SPACE}   =>  QUEX_TKN_CLAUSE_END(Lexeme);

  {CHARS}+            {QUEX_NAME(undo)(&self); self_enter_mode(&ABBREVIATION); RETURN;} 
  <<FAIL>>            =>  QUEX_TKN_UNKNOWN;
  <<EOF>>             =>  QUEX_TKN_TERMINATION;
}

mode NEW_SENTENCE:
<skip: [ \t]>
{
  on_entry                    { self_accumulator_clear();}
  on_exit                     { self_send1(QUEX_TKN_NEW_LINE, L"\n");}
  {MOST_PUNCT}                =>  QUEX_TKN_PUNCT(Lexeme);
  {PERIOD}                    =>  QUEX_TKN_SENTENCE_END(Lexeme);
  [")'}]                      =>  QUEX_TKN_FOLLOWER(Lexeme);
  {PERIOD}{2,}               {   self_send1(QUEX_TKN_SENTENCE_END, Lexeme);
                                  self_enter_mode(&NORMAL);
                                  return;
                               }
  <<FAIL>>                                             {
                                                        QUEX_NAME(undo)(&self);
                                                        self_enter_mode(&NORMAL);
                                                        return;
                                                      }
}

mode ABBREVIATION:
{
  on_entry                    { self_accumulator_clear();}
  {ABBREV}                    { self_send1(QUEX_TKN_ABBREV, Lexeme);
                                 self_enter_mode(&NORMAL); return;}
  {CHARS}+/{PERIOD}           { self_send1(QUEX_TKN_SENTENCE_END, Lexeme);
                                 self_enter_mode(&NEW_SENTENCE); return;}
  {CHARS}+                    { self_send1(QUEX_TKN_MIXED, Lexeme);
                                 self_enter_mode(&NORMAL); return;}
  {SPACE}+                    {self_enter_mode(&NORMAL); return;}
  <<FAIL>>                    { QUEX_NAME(undo)(&self);
                                self_enter_mode(&NORMAL);
                                return;}
}
